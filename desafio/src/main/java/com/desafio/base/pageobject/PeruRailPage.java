package com.desafio.base.pageobject;

import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.support.FindBy;

@DefaultUrl("https://www.perurail.com/")
public class PeruRailPage extends PageObject {

    @FindBy(xpath="//*[@id=\"destinoSelect\"]")
   public WebElementFacade comboboxDestination;

    @FindBy(xpath="//*[@id=\"rutaSelect\"]")
    public WebElementFacade comboboxRoute;

    @FindBy(xpath="//*[@id=\"cbTrenSelect\"]")
    public WebElementFacade comboboxTrain;

    @FindBy(xpath="//*[@id=\"btn_search\"]")
    public WebElementFacade bntSearch;

    @FindBy(xpath="//*[@id=\"navbar\"]/div/div[2]/div[4]/form/div/div/div/button/span")
    public WebElementFacade botonBuscar;

    @FindBy(xpath="//*[@id=\"date1\"]")
    public WebElementFacade date1;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/div/div/select[1]")
    public WebElementFacade datemonth1;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/div/div/select[1]/option[1]")
    public WebElementFacade datemonthApr;

    @FindBy(xpath = "//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[5]/td[4]/a")
    public WebElementFacade dateDay27;

    @FindBy(xpath = "//*[@id=\"btn_search_bae\"]")
    public WebElementFacade busquedafecha;

    @FindBy(xpath = "//*[@id=\"frm_viajes_bae\"]/div[3]/div[2]/div[2]/div[3]/div/div[1]/select")
    public WebElementFacade selectfirstcabintype;

    @FindBy(xpath ="//*[@id=\"continuar_bae\"]")
    public WebElementFacade primerElementoDeBusqueda;

}
