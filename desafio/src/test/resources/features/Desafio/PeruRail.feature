@PeruRail
Feature: Registro en la web de PeruRail

  @PeruRail1
  Scenario: Cliente realiza la reserva de un viaje en el tren Belmond
    Given el cliente se encuentra en la pagina de peruRail
    When el cliente hace click en combobox destino y selecciona "Cusco"
    And el cliente hace click en combobox ruta y selecciona "Puno > Cusco"
    And selecciono el tren de nombre "Andean Explorer, A Belmond Train"
    And hago click en buscar
    And selecciono fecha y cabinas a usar en el viaje de viaje

  @PeruRail2
  Scenario: Cliente realiza la reserva de un viaje en el tren Belmond
    Given el cliente se encuentra en la pagina de peruRail
    When el cliente hace click en combobox destino y selecciona "Cusco"
    And el cliente hace click en combobox ruta y selecciona "Arequipa > Puno > Cusco"
    And hago click en buscar
    And selecciono fecha y cabinas a usar en el viaje de viaje

  @PeruRail3
  Scenario: Cliente realiza la reserva de un viaje en el tren Belmond
    Given el cliente se encuentra en la pagina de peruRail
    When el cliente hace click en combobox destino y selecciona "Machu Picchu"
    And el cliente hace click en combobox ruta y selecciona "Cusco > Machu Picchu"
    And hago click en buscar
    And selecciono fecha y cabinas a usar en el viaje de viaje
