package com.desafio.base.stepDefinitions;


import com.desafio.base.steps.PeruRailPageSteps;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class PeruRailSD {

    @Steps
    private PeruRailPageSteps peruRailPageSteps;


    @Given("el cliente se encuentra en la pagina de peruRail")
    public void elClienteSeEncuentraEnLaPagina() {
        peruRailPageSteps.cargaPagina();
    }

    @When("el cliente hace click en buscador de peruRail")
    public void elClienteHaceClickEnBuscador() {
        
    }

    @When("el cliente hace click en combobox destino y selecciona {string}")
    public void elClienteHaceClickEnComboBoxYEscribe(String opcion) {
        peruRailPageSteps.seleccionaDestino(opcion);
    }

    @When("el cliente hace click en combobox ruta y selecciona {string}")
    public void elClienteHaceClickEnComboBoxYEscribe2(String opcion) {
        peruRailPageSteps.seleccionaRuta(opcion);
    }

    @When("selecciono el tren de nombre {string}")
    public void elClienteHaceClickEscribe3(String opcion) {
        peruRailPageSteps.seleccionaTren(opcion);
    }

    @And("hago click en buscar")
    public void hagoClickEnBuscar() throws InterruptedException {
        peruRailPageSteps.hagoclickenBuscar();
    }

    @Then("selecciono fecha y cabinas a usar en el viaje de viaje")
    public void eligoElPrimerElementoDeBusqueda() throws InterruptedException {
        peruRailPageSteps.hagoClickEnLaPrimeraBusqueda();
    }
}
