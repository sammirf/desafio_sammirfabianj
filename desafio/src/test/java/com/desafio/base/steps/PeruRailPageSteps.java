package com.desafio.base.steps;

import com.desafio.base.pageobject.PeruRailPage;
import org.openqa.selenium.support.ui.Select;

public class PeruRailPageSteps {

    PeruRailPage page;
    public void cargaPagina() {

        page.open();
    }

    public void seleccionaDestino(String opcion) {
        page.comboboxDestination.click();

        Select select = new Select(page.comboboxDestination);
        select.selectByVisibleText(opcion);
    }

    public void seleccionaRuta(String opcion) {
        page.comboboxRoute.click();

        Select select = new Select(page.comboboxRoute);
        select.selectByVisibleText(opcion);
    }
    public void seleccionaTren(String opcion) {
        page.comboboxTrain.click();

        Select select = new Select(page.comboboxTrain);
        select.selectByVisibleText(opcion);
    }

    public void hagoclickenBuscar() throws InterruptedException {
        page.bntSearch.click();

        Thread.sleep(10000);
    }

    public void hagoClickEnLaPrimeraBusqueda() throws InterruptedException {
        page.date1.click();
        page.datemonth1.click();

        Thread.sleep(1000);

        Select select = new Select(page.datemonth1);
        select.selectByVisibleText("Apr");


        Thread.sleep(1000);

        page.dateDay27.click();
        Thread.sleep(1000);
        page.busquedafecha.click();
        Thread.sleep(4000);

        page.selectfirstcabintype.click();

        Select select2 = new Select(page.selectfirstcabintype);
        select2.selectByVisibleText("1 CABIN");

        Thread.sleep(1000);

        page.primerElementoDeBusqueda.click();

        Thread.sleep(6000);


    }
}
